package maestro.xbox.music;

import android.text.TextUtils;
import android.util.Log;
import maestro.xbox.music.models.BaseXObject;
import maestro.xbox.music.models.XAlbum;
import maestro.xbox.music.models.XArtist;
import maestro.xbox.music.models.XTrack;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Artyom on 7/7/2015.
 */
public class XboxMusicAPI {

    public static final String TAG = XboxMusicAPI.class.getSimpleName();

    private static final String SERVICE_URL = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13";
    private static final String BASE_URL = "https://music.xboxlive.com/1/content/music/";

    private static final String CLIENT_ID = "maestro_my_music";
    private static final String CLIENT_SECRET = "maestro123d10111993rf";
    private static final String GRANT_TYPE = "client_credentials";

    private static String postData;

    static {
        postData = "client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET + "&scope=" + "http://music.xboxlive.com" + "&grant_type=" + GRANT_TYPE;
    }

    public static Object getAppToken() {
        HttpPost post = new HttpPost(SERVICE_URL);
        post.addHeader("Content-Type", "application/x-www-form-urlencoded");
        post.setEntity(new ByteArrayEntity(postData.getBytes()));
        HttpClient client = new DefaultHttpClient();
        try {
            HttpResponse response = client.execute(post);
            Log.e(TAG, "code: " + response.getStatusLine());
            if (response.getStatusLine().getStatusCode() == 200) {
                JSONObject jsonObject = new JSONObject(EntityUtils.toString(response.getEntity()));
                String accessToken = jsonObject.optString("access_token");
                if (!TextUtils.isEmpty(accessToken)) {
                    return accessToken;
                }
                //TODO: process error
            } else {
                //TODO: process error
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Object search(String token, String searchParam) {
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(BASE_URL + "search?q=daft+punk&accessToken=Bearer+" + URLEncoder.encode(token));
            HttpResponse response = client.execute(get);
            Log.e(TAG, "status: " + response.getStatusLine());


        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getFeatured(String token, String country, String genre) {
        StringBuilder builder = new StringBuilder(BASE_URL).append("spotlight?");
        appendParams(builder, country, genre, token);
        Object sendResult = sendGetRequest(builder.toString());
        if (sendResult instanceof JSONObject) {
            JSONObject jsonObject = ((JSONObject) sendResult).optJSONObject(BaseXObject.PARAM_RESULTS);
            if (jsonObject != null && jsonObject.has(BaseXObject.PARAM_ITEMS)) {
                JSONArray jArray = jsonObject.optJSONArray(BaseXObject.PARAM_ITEMS);
                final int size = jArray.length();
                final ArrayList<BaseXObject> mItems = new ArrayList<>();
                for (int i = 0; i < size; i++) {
                    mItems.add(new XAlbum(jArray.optJSONObject(i).optJSONObject(BaseXObject.PARAM_ALBUM)));
                }
                return mItems;
            }
        }
        return null;
    }

    public static Object getNewReleases(String token, String country, String genre) {
        StringBuilder builder = new StringBuilder(BASE_URL).append("newreleases?");
        appendParams(builder, country, genre, token);
        Object sendResult = sendGetRequest(builder.toString());
        if (sendResult instanceof JSONObject) {
            JSONObject jsonObject = ((JSONObject) sendResult).optJSONObject(BaseXObject.PARAM_RESULTS);
            if (jsonObject != null && jsonObject.has(BaseXObject.PARAM_ITEMS)) {
                JSONArray jArray = jsonObject.optJSONArray(BaseXObject.PARAM_ITEMS);
                final int size = jArray.length();
                final ArrayList<BaseXObject> mItems = new ArrayList<>();
                for (int i = 0; i < size; i++) {
                    mItems.add(new XAlbum(jArray.optJSONObject(i).optJSONObject(BaseXObject.PARAM_ALBUM)));
                }
                return mItems;
            }
        }
        return null;
    }


    public static Object getAlbums(String token, String country, String genre) {
        StringBuilder builder = new StringBuilder(BASE_URL).append("catalog/albums/browse?");
        appendParams(builder, country, genre, token);
        Object sendResult = sendGetRequest(builder.toString());
        if (sendResult instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) sendResult;
            if (jsonObject.has(BaseXObject.PARAM_ALBUMS)) {
                jsonObject = jsonObject.optJSONObject(BaseXObject.PARAM_ALBUMS);
                if (jsonObject.has(BaseXObject.PARAM_ITEMS)) {
                    JSONArray jArray = jsonObject.optJSONArray(BaseXObject.PARAM_ITEMS);
                    final int size = jArray.length();
                    final ArrayList<BaseXObject> mItems = new ArrayList<>();
                    for (int i = 0; i < size; i++) {
                        mItems.add(new XAlbum(jArray.optJSONObject(i)));
                    }
                    return mItems;
                }
            }
        }
        return null;
    }


    public static Object getTracks(String token, String country, String genre) {
        StringBuilder builder = new StringBuilder(BASE_URL).append("catalog/tracks/browse?");
        appendParams(builder, country, genre, token);
        Object sendResult = sendGetRequest(builder.toString());
        if (sendResult instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) sendResult;
            if (jsonObject.has(BaseXObject.PARAM_TRACKS)) {
                jsonObject = jsonObject.optJSONObject(BaseXObject.PARAM_TRACKS);
                if (jsonObject.has(BaseXObject.PARAM_ITEMS)) {
                    JSONArray jArray = jsonObject.optJSONArray(BaseXObject.PARAM_ITEMS);
                    final int size = jArray.length();
                    final ArrayList<BaseXObject> mItems = new ArrayList<>();
                    for (int i = 0; i < size; i++) {
                        mItems.add(new XTrack(jArray.optJSONObject(i)));
                    }
                    return mItems;
                }
            }
        }
        return null;
    }


    public static Object getArtists(String token, String country, String genre) {
        StringBuilder builder = new StringBuilder(BASE_URL).append("catalog/artists/browse?");
        appendParams(builder, country, genre, token);
        Object sendResult = sendGetRequest(builder.toString());
        if (sendResult instanceof JSONObject) {
            JSONObject jsonObject = (JSONObject) sendResult;
            if (jsonObject.has(BaseXObject.PARAM_ARTISTS)) {
                jsonObject = jsonObject.optJSONObject(BaseXObject.PARAM_ARTISTS);
                if (jsonObject.has(BaseXObject.PARAM_ITEMS)) {
                    JSONArray jArray = jsonObject.optJSONArray(BaseXObject.PARAM_ITEMS);
                    final int size = jArray.length();
                    final ArrayList<BaseXObject> mItems = new ArrayList<>();
                    for (int i = 0; i < size; i++) {
                        mItems.add(new XArtist(jArray.optJSONObject(i)));
                    }
                    return mItems;
                }
            }
        }
        return null;
    }

    private static void appendParams(StringBuilder builder, String country, String genre, String token) {
        boolean and = false;
        if (!TextUtils.isEmpty(country)) {
            if (and)
                builder.append("&");
            builder.append("country=" + country);
            and = true;
        }
        if (!TextUtils.isEmpty(genre)) {
            if (and)
                builder.append("&");
            builder.append("genre=" + genre);
            and = true;
        }
        builder.append("&accessToken=Bearer+").append(URLEncoder.encode(token));
    }

    private static Object sendGetRequest(String request) {
        HttpGet get = new HttpGet(request);
        return sendRequest(get);
    }

    private static Object sendPostRequest() {
        return null;
    }

    private static Object sendRequest(HttpRequestBase requestBase) {
        HttpClient client = new DefaultHttpClient();
        try {
            HttpResponse response = client.execute(requestBase);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                return new JSONObject(EntityUtils.toString(response.getEntity()));
            }
            //TODO: process error
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


}
