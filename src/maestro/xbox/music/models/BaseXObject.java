package maestro.xbox.music.models;

import android.text.TextUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Artyom on 7/7/2015.
 */
public abstract class BaseXObject {

    public static final String PARAM_ID = "Id";
    public static final String PARAM_NAME = "Name";
    public static final String PARAM_IMAGE_URL = "ImageUrl";
    public static final String PARAM_LINK = "Link";
    public static final String PARAM_SOURCE = "Source";
    public static final String PARAM_COMPATIBLE_SOURCES = "CompatibleSources";
    public static final String PARAM_RELEASE_DATE = "ReleaseDate";
    public static final String PARAM_DURATION = "Duration";
    public static final String PARAM_TRACK_COUNT = "TrackCount";
    public static final String PARAM_TRACK_NUMBER = "TrackNumber";
    public static final String PARAM_IS_EXPLICIT = "IsExplicit";
    public static final String PARAM_LABEL_NAME = "LabelName";
    public static final String PARAM_SUB_TITLE = "Subtitle";

    public static final String PARAM_RESULTS = "Results";
    public static final String PARAM_ITEMS = "Items";
    public static final String PARAM_ARTISTS = "Artists";
    public static final String PARAM_ARTIST = "Artist";
    public static final String PARAM_ALBUMS = "Albums";
    public static final String PARAM_ALBUM = "Album";
    public static final String PARAM_TRACKS = "Tracks";
    public static final String PARAM_GENRES = "Genres";
    public static final String PARAM_SUB_GENRES = "Subgenres";
    public static final String PARAM_RIGHTS = "Rights";

    JSONObject jObj;

    public BaseXObject(JSONObject jObject) {
        jObj = jObject;
        prepare();
    }

    public String getId() {
        return jObj.optString(PARAM_ID);
    }

    public String getName() {
        return jObj.optString(PARAM_NAME);
    }

    public String getImageUrl() {
        return jObj.optString(PARAM_IMAGE_URL);
    }

    public String getImageUrl(int width, int height) {
        String url = getImageUrl();
        if (!TextUtils.isEmpty(url)) {
            return new StringBuilder(url).append("&w=").append(width).append("&h=").append(height).toString();
        }
        return null;
    }

    public String getLink() {
        return jObj.optString(PARAM_LINK);
    }

    public String getSource() {
        return jObj.optString(PARAM_SOURCE);
    }

    public String getComatibleSources() {
        return jObj.optString(PARAM_COMPATIBLE_SOURCES);
    }

    public final String[] fromJArray(String param) {
        if (jObj.has(param)) {
            JSONArray jArray = jObj.optJSONArray(param);
            final int size = jArray.length();
            if (size > 0) {
                String[] strings = new String[size];
                for (int i = 0; i < size; i++) {
                    strings[i] = jArray.optString(i);
                }
                return strings;
            }
        }
        return null;
    }

    public abstract void prepare();

}
