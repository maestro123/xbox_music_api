package maestro.xbox.music.models;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Artyom on 7/7/2015.
 */
public class XAlbum extends XListItem {

    private XArtist[] mArtist;

    public XAlbum(JSONObject jObject) {
        super(jObject);
    }

    @Override
    public void prepare() {
        super.prepare();
        if (jObj.has(PARAM_ARTISTS)) {
            JSONArray jArray = jObj.optJSONArray(PARAM_ARTISTS);
            final int size = jArray.length();
            if (size > 0) {
                mArtist = new XArtist[size];
                for (int i = 0; i < size; i++) {
                    mArtist[i] = new XArtist(jArray.optJSONObject(i).optJSONObject(PARAM_ARTIST));
                }
            }
        }
    }

    protected String getReleaseDate() {
        return jObj.optString(PARAM_RELEASE_DATE);
    }

    protected String getDuration() {
        return jObj.optString(PARAM_DURATION);
    }

    protected int getTrackCount() {
        return jObj.optInt(PARAM_TRACK_COUNT);
    }

    protected boolean IsExplicit() {
        return jObj.optBoolean(PARAM_IS_EXPLICIT);
    }

    protected String getLabelName() {
        return jObj.optString(PARAM_LABEL_NAME);
    }

}
