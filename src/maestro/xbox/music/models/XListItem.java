package maestro.xbox.music.models;

import org.json.JSONObject;

/**
 * Created by Artyom on 7/7/2015.
 */
public class XListItem extends BaseXObject {

    private String[] mGenres;
    private String[] mSubGenres;

    public XListItem(JSONObject jObject) {
        super(jObject);
    }

    @Override
    public void prepare() {
        mGenres = fromJArray(PARAM_GENRES);
        mSubGenres = fromJArray(PARAM_SUB_GENRES);
    }
}
