package maestro.xbox.music.models;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Artyom on 7/7/2015.
 */
public class XTrack extends XListItem {

    private XArtist[] mArtist;
    private XAlbum mAlbum;
    private String[] mRights;

    public XTrack(JSONObject jObject) {
        super(jObject);
    }

    @Override
    public void prepare() {
        super.prepare();
        if (jObj.has(PARAM_ARTISTS)) {
            JSONArray jArray = jObj.optJSONArray(PARAM_ARTISTS);
            final int size = jArray.length();
            if (size > 0) {
                mArtist = new XArtist[size];
                for (int i = 0; i < size; i++) {
                    mArtist[i] = new XArtist(jArray.optJSONObject(i).optJSONObject(PARAM_ARTIST));
                }
            }
        }
        if (jObj.has(PARAM_ALBUM)) {
            mAlbum = new XAlbum(jObj.optJSONObject(PARAM_ALBUM));
        }
        mRights = fromJArray(PARAM_RIGHTS);
    }

    protected String getReleaseDate() {
        return jObj.optString(PARAM_RELEASE_DATE);
    }

    protected String getDuration() {
        return jObj.optString(PARAM_DURATION);
    }

    protected int getTrackNumber() {
        return jObj.optInt(PARAM_TRACK_NUMBER);
    }

    protected String getSubTitle() {
        return jObj.optString(PARAM_SUB_TITLE);
    }

    protected boolean IsExplicit() {
        return jObj.optBoolean(PARAM_IS_EXPLICIT);
    }

}
